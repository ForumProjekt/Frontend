# Frontend

## MDBootstrap Components Info
Bootstrap Componenten Liste auf [mdbootstrap.com](https://mdbootstrap.com/angular/)

## Angular CLI

1. Componente Erstellen: `ng g c components/<name>`
2. Klasse/Model erstellen: `ng g class models/<name>`
3. Service erstellen: `ng g s services/<name>`
4. Pipe erstellen: `ng g p pipes/<name>`
5. Interface erstellen: `ng g i interfaces/<name>`
6. Modul erstellen: `ng g m modules/<name>`
7. Direktive erstellen: `ng g d directives/<name>`

## Testing

Zum Testen des Frontend mit [Karma](https://karma-runner.github.io) `ng test` ausführen

## Build

Zum Hochladen auf den Server die App bauen mit
`ng build -prod -bh="/"`
und gesamten Inhalt aus Ordner `/dist` hochladen.

## Angular CLI Readme

Für weiter Hilfe `ng help` oder
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md)
auf GitHub.

## Serververwaltung
WebGUI zur Verwaltung von Domain, Datenbanken, E-Mails unter https://host1.unbekannt3.eu:8080  
Benutzer: `projekt`, Passwort: `SZUT12345`
___
FTP Daten für Build Upload auf www.pointless.ga:  
Host: `host1.unbekannt3.eu`, Benutzer: `projektWEB`, Passwort `SZUT12345`
