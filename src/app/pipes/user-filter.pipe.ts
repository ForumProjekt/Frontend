import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models/user';

@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {

  transform(users: User[], suchtext: string): User[] {

    if (!suchtext) {
      return users;
    }
    users = users.filter(u =>
      u.username.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.uuid.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.email.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.firstname.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.lastname.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.location.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || u.homepage.toUpperCase().indexOf(suchtext.toUpperCase()) >= 0
      || String(u.registeredSince) === suchtext
      || String(u.birthdate) === suchtext
      || String(u.level) === suchtext
      || String(u.rank) === suchtext

    );
    return users;
  }

}
