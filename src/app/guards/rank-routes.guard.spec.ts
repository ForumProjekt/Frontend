import { TestBed, async, inject } from '@angular/core/testing';

import { RankRoutesGuard } from './rank-routes.guard';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('RankRoutesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    });
  });

  it('should ...', inject([RankRoutesGuard], (guard: RankRoutesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
