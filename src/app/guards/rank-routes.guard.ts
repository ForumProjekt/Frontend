import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import { SystemAlertsService } from '../services/system-alerts.service';
import { Location } from '@angular/common';
const jwtDecode = require('jwt-decode');

@Injectable()
export class RankRoutesGuard implements CanActivate {

  constructor(
    public authService: AuthService,
    private systemAlertsService: SystemAlertsService,
    public router: Router,
    private location: Location
  ) { }
  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (this.authService.loggedIn()) {
      // get requiered rank from route config
      const expectedRank = route.data.expectedRankID;
      let token;
      let tokenPayload;
      // get JWT and decode it
      token = localStorage.getItem('id_token');
      try {
        tokenPayload = jwtDecode(token);
      } catch (error) {
        console.log('[ERROR] [RouteGuard] Can\'t decode JWT Payload!: ' + error);
      }

      // DEBUG: print decoded JWT to Console
      console.log(JSON.stringify(tokenPayload));

      // check JWT Payload for rank property and compare it with expected rank
      console.log('[RouteGuard] Expected Rank for Route: ' + expectedRank + ', current User Rank: ' + tokenPayload.rank);
      if (tokenPayload && tokenPayload.rank >= expectedRank) {
        return true;
      } else {
        if (!this.router.navigated || this.location.back() === undefined) {
          this.router.navigate(['home']);
          this.systemAlertsService.display('Du hast keine Rechte diese Seite zu betreten!', 'danger');
        } else {
          this.location.back();
          this.systemAlertsService.display('Du hast keine Rechte diese Seite zu betreten! - Leite auf vorherige Seite zurück', 'danger');
        }
        return false;
      }

    } else {
      this.router.navigate(['login']);
      this.systemAlertsService.display('Du musst eingeloggt sein, um diese Seite zu betreten!', 'danger', 'Fehler!');
      return false;
    }
  }
}
