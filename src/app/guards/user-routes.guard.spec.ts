import { TestBed, async, inject } from '@angular/core/testing';

import { UserRoutesGuard } from './user-routes.guard';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('UserRoutesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    });
  });

  it('should ...', inject([UserRoutesGuard], (guard: UserRoutesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
