import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { AuthService } from '../services/auth.service';
import { SystemAlertsService } from '../services/system-alerts.service';

@Injectable()
export class UserRoutesGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private systemAlertsService: SystemAlertsService,
    private router: Router
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    if (!this.authService.loggedIn()) {
      this.router.navigate(['login']);
      this.systemAlertsService.display('Du musst eingeloggt sein, um diese Seite zu betreten!', 'danger', 'Fehler!');
      return false;
    }
    return true;

  }
}
