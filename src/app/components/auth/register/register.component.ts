import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../../models/user';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  fb: FormGroup;
  constructor(
    private authService: AuthService
  ) { }

  user: User = new User();

  save() {
    this.user.birthdate = new Date(this.user.birthdate);
    this.authService.register(this.user);
  }

  ngOnInit() {
  }

}
