import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorBlur = false;

  constructor(
    private authService: AuthService
  ) { }

  user: User = new User('0');

  login() {
    // console.log('[DEBUG] entered username/password: ' + this.user.username + '/' + this.user.password);
    this.authService.login(this.user);
  }

  ngOnInit() {
  }

}
