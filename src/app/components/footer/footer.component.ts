import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(
    private authService: AuthService
  ) { }

  get loggedIn(): boolean {

    return this.authService.loggedIn();

  }

  get currentYear() {
    return new Date().getFullYear();
  }

  ngOnInit() {
  }

}
