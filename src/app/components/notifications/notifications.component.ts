import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../services/notifications.service';
import { Notification } from '../../models/notification';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {

  private _notification: Notification[] = [];

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this._notification = this.notificationService.notifications;
  }

  get notifications() {
    return this._notification;
  }

  syncNotifications() {
    this._notification = this.notificationService.notifications;
    console.log(JSON.stringify(this.notifications[0].notificationDate));
  }

}
