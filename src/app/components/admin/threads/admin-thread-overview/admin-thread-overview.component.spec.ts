import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminThreadOverviewComponent } from './admin-thread-overview.component';
import { AppModule } from '../../../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('AdminThreadOverviewComponent', () => {
  let component: AdminThreadOverviewComponent;
  let fixture: ComponentFixture<AdminThreadOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminThreadOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
