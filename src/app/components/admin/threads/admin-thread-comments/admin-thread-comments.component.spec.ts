import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminThreadCommentsComponent } from './admin-thread-comments.component';
import { AppModule } from '../../../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('AdminThreadCommentsComponent', () => {
  let component: AdminThreadCommentsComponent;
  let fixture: ComponentFixture<AdminThreadCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminThreadCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
