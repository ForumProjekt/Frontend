import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminThreadCategoriesComponent } from './admin-thread-categories.component';
import { AppModule } from '../../../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('AdminThreadCategoriesComponent', () => {
  let component: AdminThreadCategoriesComponent;
  let fixture: ComponentFixture<AdminThreadCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminThreadCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
