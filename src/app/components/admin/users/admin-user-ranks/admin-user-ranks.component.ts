import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { RankService } from '../../../../services/rank.service';
import { ModalService } from '../../../../services/modal.service';
import { Rank } from '../../../../models/rank';

@Component({
  selector: 'app-admin-user-ranks',
  templateUrl: './admin-user-ranks.component.html',
  styleUrls: ['./admin-user-ranks.component.scss']
})
export class AdminUserRanksComponent implements OnInit {

  public filter = '';
  public maxSize = 7;
  public directionLinks = true;
  public autoHide = false;
  public config: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };
  public labels: any = {
    previousLabel: 'Vorherige',
    nextLabel: 'Nächste',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'seite',
    screenReaderCurrentLabel: `You're on page`
  };

  constructor(
    private rankService: RankService,
    private modalService: ModalService
  ) { }

  get ranks() {
    return this.rankService.getRanks;
  }

  editRank(rankEntry: Rank) {
    // this.userService.openAdminUserEdit(userEntry);
    this.modalService.openAdminEditRankModal(rankEntry);
    // this.showModal();
  }

  onPageChange(number: number) {
    // console.log('Userlist changed to page ', number);
    this.config.currentPage = number;
  }

  updateList() {
    this.rankService.updateCache();
  }

  get rotateActive() {
    return this.rankService.rotateActive;
  }

  ngOnInit() {
  }

}
