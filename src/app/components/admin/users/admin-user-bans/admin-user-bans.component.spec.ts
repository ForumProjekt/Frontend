import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUserBansComponent } from './admin-user-bans.component';
import { AppModule } from '../../../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('AdminUserBansComponent', () => {
  let component: AdminUserBansComponent;
  let fixture: ComponentFixture<AdminUserBansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUserBansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
