import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { HttpClient } from '@angular/common/http';
import { ModalService } from '../../../../services/modal.service';
import { BanService } from '../../../../services/ban.service';
import { Ban } from '../../../../models/ban';

@Component({
  selector: 'app-admin-user-bans',
  templateUrl: './admin-user-bans.component.html',
  styleUrls: ['./admin-user-bans.component.scss']
})
export class AdminUserBansComponent implements OnInit {

  public filter = '';
  public maxSize = 7;
  public directionLinks = true;
  public autoHide = false;
  public config: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };
  public labels: any = {
    previousLabel: 'Vorherige',
    nextLabel: 'Nächste',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'seite',
    screenReaderCurrentLabel: `You're on page`
  };
  public editBanModalActive = false;

  constructor(
    private banService: BanService,
    private modalService: ModalService,
    private http: HttpClient
  ) { }

  get bans() {
    return this.banService.getBans;
  }

  get currentlyEditingBan() {
    return this.banService.currentlyEditingBan;
  }

  showModal(): void {
    this.editBanModalActive = true;
  }

  hideModal(): void {
    this.banService.openAdminBanEdit(undefined);
    this.editBanModalActive = false;
  }

  editBan(banEntry: Ban) {
    // this.userService.openAdminUserEdit(userEntry);
    this.modalService.openAdminEditBanModal(banEntry);
    // this.showModal();
  }

  deleteBan(banEntry: Ban) {
    this.banService.deleteBan(banEntry);
  }

  onPageChange(number: number) {
    // console.log('Userlist changed to page ', number);
    this.config.currentPage = number;
  }

  updateList() {
    this.banService.updateCache();
  }

  get rotateActive() {
    return this.banService.rotateActive;
  }

  ngOnInit() {
  }

}
