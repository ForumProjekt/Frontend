import { ChangeDetectionStrategy, Component, Input, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PaginationInstance } from 'ngx-pagination';
import { UserService } from '../../../../services/user.service';
import { User } from '../../../../models/user';
import { ModalDirective } from 'angular-bootstrap-md';
import { ModalService } from '../../../../services/modal.service';

@Component({
  selector: 'app-admin-user-overview',
  templateUrl: './admin-user-overview.component.html',
  styleUrls: ['./admin-user-overview.component.scss']
})
export class AdminUserOverviewComponent implements OnInit {

  public filter = '';
  public maxSize = 7;
  public directionLinks = true;
  public autoHide = false;
  public config: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };
  public labels: any = {
    previousLabel: 'Vorherige',
    nextLabel: 'Nächste',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'seite',
    screenReaderCurrentLabel: `You're on page`
  };
  public editUserModalActive = false;

  constructor(
    private userService: UserService,
    private modalService: ModalService,
    private http: HttpClient
  ) { }

  get users() {
    return this.userService.getUsers;
  }

  get currentlyEditingUser() {
    return this.userService.currentlyEditingUser;
  }

  showModal(): void {
    this.editUserModalActive = true;
  }

  hideModal(): void {
    this.userService.openAdminUserEdit(undefined);
    this.editUserModalActive = false;
  }

  editUser(userEntry: User) {
    // this.userService.openAdminUserEdit(userEntry);
    this.modalService.openAdminEditUserModal(userEntry);
    // this.showModal();
  }

  deleteUser(userEntry: User) {

  }

  onPageChange(number: number) {
    // console.log('Userlist changed to page ', number);
    this.config.currentPage = number;
  }

  updateList() {
    this.userService.updateCache();
  }

  get rotateActive() {
    return this.userService.rotateActive;
  }

  ngOnInit() {
  }

}
