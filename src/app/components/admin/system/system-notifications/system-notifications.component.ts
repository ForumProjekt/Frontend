import { Component, OnInit } from '@angular/core';
import { PaginationInstance } from 'ngx-pagination';
import { UserService } from '../../../../services/user.service';
import { ModalService } from '../../../../services/modal.service';
import { HttpClient } from '@angular/common/http';
import { NotificationService } from '../../../../services/notifications.service';
import { Notification } from '../../../../models/notification';


@Component({
  selector: 'app-system-notifications',
  templateUrl: './system-notifications.component.html',
  styleUrls: ['./system-notifications.component.scss']
})
export class SystemNotificationsComponent implements OnInit {

  public filter = '';
  public maxSize = 7;
  public directionLinks = true;
  public autoHide = false;
  public config: PaginationInstance = {
    id: 'advanced',
    itemsPerPage: 10,
    currentPage: 1
  };
  public labels: any = {
    previousLabel: 'Vorherige',
    nextLabel: 'Nächste',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'seite',
    screenReaderCurrentLabel: `You're on page`
  };
  public editUserModalActive = false;

  public _notification: Notification = new Notification();


  constructor(
    private notificationService: NotificationService,
    private http: HttpClient
  ) { }

  ngOnInit() {
  }

  get notification() {
    return this.notificationService.notifications;
  }

  deleteNotification(id: number) {
    this.deleteNotification(id);
  }

  addNotification() {
    console.log(this._notification.notificationText);
    this.notificationService.addNotification(this._notification);
  }

  onPageChange(number: number) {
    // console.log('Userlist changed to page ', number);
    this.config.currentPage = number;
  }

  updateList() {
    this.notificationService.syncNotifications();
  }

}
