import { Component, OnInit } from '@angular/core';
import { ThreadsService } from '../../../../services/threads.service';
import { UserService } from '../../../../services/user.service';
import { CommentService } from '../../../../services/comment.service';
import { SystemService } from '../../../../services/system.service';

@Component({
  selector: 'app-system-overview',
  templateUrl: './system-overview.component.html',
  styleUrls: ['./system-overview.component.scss']
})
export class SystemOverviewComponent implements OnInit {

  constructor(
    private threadService: ThreadsService,
    private userService: UserService,
    private commentService: CommentService,
    private systemService: SystemService
  ) { }

  get cpuThreads() {
    return this.systemService.cpuThreads;
  }

  get maxMem() {
    const mem = '' + this.systemService.maxMem;
    const text = mem.split(/[ ,.]+/);
    return text[0];
  }

  get usedMem() {
    const mem = '' + this.systemService.usedMem;
    const text = mem.split(/[ ,.]+/);
    return text[0];
  }

  get freeMem() {
    const mem = '' + this.systemService.freeMem;
    const text = mem.split(/[ ,.]+/);
    return text[0];
  }

  get hostOS() {
    return this.systemService.hostOS;
  }

  get backendUptime() {
    return this.systemService.backendUptime;
  }

  get backendAvgLoad() {
    const avg1 = this.systemService.backendAvgLoad.avg1 + '';
    const avg10 = this.systemService.backendAvgLoad.avg10 + '';
    const avg15 = this.systemService.backendAvgLoad.avg15 + '';

    const string = avg1.substr(0, 2) + '/' + avg10.substr(0, 2) + '/' + avg15.substr(0, 2);
    return string;
  }

  get allThreads() {
    return this.threadService.getThreads.length;
  }

  get allUsers() {
    return this.userService.getUsers.length;
  }

  ngOnInit() {
  }

}
