import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-admin-menu',
  templateUrl: './admin-menu.component.html',
  styleUrls: ['./admin-menu.component.scss']
})
export class AdminMenuComponent implements OnInit {
  private user = new User();

  constructor(
    private uS: UserService
  ) {
    this.uS.activeUserObservable.subscribe(user => {
      this.user = user;
    });
  }

  get userLevel() {
    return this.user.rank;
  }

  ngOnInit() {
  }

}
