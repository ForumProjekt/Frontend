import { Component, OnInit } from '@angular/core';
import { ThreadsService } from '../../services/threads.service';
import { Thread } from '../../models/thread';
import { UserService } from '../../services/user.service';
import { Observable } from 'rxjs/Observable';
import { User } from '../../models/user';

@Component({
  selector: 'app-new-threads',
  templateUrl: './new-threads.component.html',
  styleUrls: ['./new-threads.component.scss']
})
export class NewThreadsComponent implements OnInit {

  _tags: string;
  thread: Thread = new Thread(0, '', '', 0, 0, 0, new Date(), 'UUID_DUMMY!');
  currentUser: User;

  constructor(
    public threadService: ThreadsService,
    public userService: UserService
  ) {
    this.userService.activeUserObservable.subscribe(user => {
      this.currentUser = user;
    });
   }

  createThread() {
    this.thread.userID = this.currentUser.uuid;
    let tagLabelsArray: string[] = [];

    if (this._tags)  {
      try {
        tagLabelsArray = this._tags.split(',');
      } catch (err) {
        tagLabelsArray[0] = this._tags;
        console.log('can\'t split tag string to array - only one tag given?');
      }
      tagLabelsArray = tagLabelsArray.map(Function.prototype.call, String.prototype.trim);
    } else {
      this._tags = '';
    }

    console.log(tagLabelsArray);
    console.log(JSON.stringify(this.thread));
    this.threadService.newThread(this.thread, tagLabelsArray);

    this.thread = new Thread(0, '', '', 0, 0, 0, new Date(), 'UUID_DUMMY!');
    this._tags = '';

  }

  ngOnInit() {
  }

}
