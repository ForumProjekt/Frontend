import { Component, OnInit } from '@angular/core';
import { CommentService } from '../../services/comment.service';
import { ActivatedRoute } from '@angular/router';
import { Comment } from '../../models/comment';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  public commentArray: Comment[];
  private threadID;
  private user;

  constructor(
    private route: ActivatedRoute,
    private cServ: CommentService,
    private uServ: UserService
  ) {
    this.route.params.subscribe(params => {
      if (params['threadID']) {
        this.threadID = +params['threadID']; // (+) converts string 'id' to a number
        console.log('threadID: ' + this.threadID);
        cServ.getComments(this.threadID);
      }
    });
  }

  get comments() {
    return this.cServ.comments;
  }

  get commentByID() {
    if (this.comments !== undefined) {
      this.commentArray = this.comments;
      console.log(this.commentArray[0].commentText);
      return this.commentArray; // nicht backend zugriff returnen in comment service!!!!!!!!!
    } else {
      return [];
    }
  }

  // get commentByID() {
  //   if (this.comments !== undefined || this.commentArray === undefined) {
  //     this.commentArray = this.comments;
  //     console.log(this.commentArray[0].commentText);
  //     return this.commentArray; //nicht backend zugriff returnen in comment service!!!!!!!!!
  //   } else if (this.commentArray !== undefined) {
  //     return this.commentArray;
  //   } else {
  //     return [];
  //   }

  // }

  ngOnInit() {
  }

}
