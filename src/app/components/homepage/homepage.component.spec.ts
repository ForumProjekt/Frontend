import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageComponent } from './homepage.component';
import { By, element, browser } from 'protractor';
import { AppModule } from '../../app.module';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';

describe('HomepageComponent', () => {
  let component: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have an list with the trends of the board', () => {
    expect(fixture.nativeElement.querySelector('div#trends').textContent).toContain('Aktuelle Trends');
  });

  it('should have an list with interesting threads for me', () => {
    expect(fixture.nativeElement.querySelector('div#interestingThreads').textContent).toContain('Interessante Beiträge');
  });

  it('should have an lorem ipsum news in news list', () => {
    expect(fixture.nativeElement.querySelector('div#news').textContent).toContain('Lorem ipsum dolor sit amet');
  });

  it('should have an livechat with an lorem ipsum message at 19:35 from User1', () => {
    expect(fixture.nativeElement.querySelector('div#livechat').textContent).toContain('User1');
    expect(fixture.nativeElement.querySelector('div#livechat').textContent).toContain('19:53');
    expect(fixture.nativeElement.querySelector('div#livechat').textContent).toContain('Lorem ipsum dolor sit amet');
  });
});
