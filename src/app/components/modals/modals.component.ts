import { Component, OnInit } from '@angular/core';
import { ModalService } from '../../services/modal.service';
import { User } from '../../models/user';
import { IMyDpOptions } from 'mydatepicker';
import * as moment from 'moment';
import { Rank } from '../../models/rank';
import { Ban } from '../../models/ban';

@Component({
  selector: 'app-modals',
  templateUrl: './modals.component.html',
  styleUrls: ['./modals.component.scss']
})
export class ModalsComponent implements OnInit {

  model: any = { date: { year: 2018, month: 10, day: 9 } };
  datepickObj: any;
  datepickBanExpiresObj: any;
  myDatePickerOptions: IMyDpOptions = {
    dateFormat: 'dd.mm.yyyy'
  };

  constructor(
    private modalService: ModalService
  ) {
  }

  get eUser(): User {
    return this.modalService.adminEditUser;

  }
  get eRank(): Rank {
    return this.modalService.adminEditRank;
  }

  get eBan(): Ban {
    return this.modalService.adminEditBan;
  }

  get loadingModal() {
    return this.modalService.loadingModalStatus;
  }

  get editUserModal() {
    return this.modalService.adminEditUserModalStatus;
  }

  get editRankModal() {
    return this.modalService.adminEditRankModalStatus;
  }

  get editBanModal() {
    return this.modalService.adminEditBanModalStatus;
  }

  closeAdminEditUserModal() {
    this.modalService.closeAdminEditUserModal();
  }

  closeAdminEditRankModal() {
    this.modalService.closeAdminEditRankModal();
  }

  closeAdminEditBanModal() {
    this.modalService.closeAdminEditBanModal();
  }

  saveAdminEditUserModal() {
    if (this.datepickObj != null) {
      const dateStr = `${this.datepickObj['date']['year']}/${this.datepickObj['date']['month']}/${this.datepickObj['date']['day']}`;
      this.eUser.birthdate = new Date(dateStr);
    }
    this.modalService.saveAdminEditUser();
  }

  saveAdminEditRankModal() {
    this.modalService.saveAdminEditRank();
  }

  saveAdminEditBanModal() {
    if (this.datepickBanExpiresObj != null) {
      const dateStr = `${this.datepickBanExpiresObj['date']['year']}/${this.datepickBanExpiresObj['date']['month']}/${this.datepickBanExpiresObj['date']['day']}`;
      this.eBan.expiresAt = new Date(dateStr);
    }
    this.modalService.saveAdminEditBan();
  }

  ngOnInit() {
  }

}
