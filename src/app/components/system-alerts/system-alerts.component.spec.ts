import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SystemAlertsComponent } from './system-alerts.component';
import { AppModule } from '../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('SystemAlertsComponent', () => {
  let component: SystemAlertsComponent;
  let fixture: ComponentFixture<SystemAlertsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SystemAlertsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
