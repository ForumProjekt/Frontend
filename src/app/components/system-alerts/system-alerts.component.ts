import { Component, OnInit } from '@angular/core';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { SystemAlertsService } from '../../services/system-alerts.service';

@Component({
  selector: 'app-system-alerts',
  templateUrl: './system-alerts.component.html',
  animations: [
    trigger(
      'messageBoxAnimation',
      [
        transition(
          ':enter', [
            // style({ transform: 'translateX(100%)', opacity: 0 }),
            // animate('500ms', style({ transform: 'translateX(0)', 'opacity': 1 }))
            style({ opacity: 0 }),
            animate('500ms', style({ 'opacity': 1 }))
          ]
        ),
        transition(
          ':leave', [
            style({ 'opacity': 1 }),
            animate('1000ms', style({ 'opacity': 0, 'width': '100%' }))
          ]
        )]
    )
  ],
  styleUrls: ['./system-alerts.component.scss']
})
export class SystemAlertsComponent implements OnInit {

  constructor(
    private saService: SystemAlertsService
  ) { }

  get message(): string {
    return this.saService.text;
  }

  get title(): string {
    return this.saService.title;
  }

  get type(): string {
    return this.saService.type;
  }

  ngOnInit() {
  }

}
