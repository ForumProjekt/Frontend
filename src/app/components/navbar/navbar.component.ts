import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  private user;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router
  ) {
    this.userService.activeUserObservable.subscribe(user => {
      this.user = user;
    });
  }

  get loggedIn() {
    return this.authService.loggedIn();
  }

  get modRank() {
    if (this.user.rank >= 10) {
      return true;
    } else {
      return false;
    }
  }

  login() {
    this.authService.currentPath = this.router.url;
  }

  logout() {
    this.authService.logout();
  }

  ngOnInit() {
  }

}
