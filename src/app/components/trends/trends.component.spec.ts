import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrendsComponent } from './trends.component';
import { AppModule } from '../../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('TrendsComponent', () => {
  let component: TrendsComponent;
  let fixture: ComponentFixture<TrendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('421 / 3 * 12 should be 1684', () => {
    expect(component.rechnung(421, 3, 12)).toBe(1684);
  });

  it('586 / 5 * 112 should be 13126.4', () => {
    expect(component.rechnung(586, 5, 112)).toBe(13126.4);
  });

  it('216 / 6 * 4 should be 144', () => {
    expect(component.rechnung(216, 6, 4)).toBe(144);
  });

});
