import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  // asd
  public currentUser: User;

  constructor(
    private userService: UserService
  ) {
    this.userService.activeUserObservable.subscribe(user => this.currentUser = user);
  }



  ngOnInit() {
  }

}
