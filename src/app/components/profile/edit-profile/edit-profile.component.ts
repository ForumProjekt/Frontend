import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  public user: User = new User();



  constructor(
    private userService: UserService
  ) {
    this.userService.activeUserObservable.subscribe(user => {
      this.user = user;
    });
  }



  save() {
    console.log(this.user.username);
    // if (!this.user.uuid) {
    //   this.user.uuid = this.userService.activeUser.uuid;
    // }
    // if (!this.user.username) {
    //   this.user.username = this.userService.activeUser.username;
    // }
    // if (!this.user.email) {
    //   this.user.email = this.userService.activeUser.email;
    // }
    // if (!this.user.firstname) {
    //   this.user.firstname = this.userService.activeUser.firstname;
    // }
    // if (!this.user.lastname) {
    //   this.user.lastname = this.userService.activeUser.lastname;
    // }
    // if (!this.user.birthdate) {
    //   this.user.birthdate = this.userService.activeUser.birthdate;
    // }
    // if (!this.user.biography) {
    //   this.user.biography = this.userService.activeUser.biography;
    // }
    // if (!this.user.location) {
    //   this.user.location = this.userService.activeUser.location;
    // }
    // if (!this.user.homepage) {
    //   this.user.homepage = this.userService.activeUser.homepage;
    // }
    // if (!this.user.level) {
    //   this.user.level = this.userService.activeUser.level;
    // }
    // if (!this.user.rank) {
    //   this.user.rank = this.userService.activeUser.rank;
    // }

    this.userService.updateUserProfile(this.user);
  }

  ngOnInit() {
  }

}
