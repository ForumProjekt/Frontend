import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-aboutme',
  templateUrl: './aboutme.component.html',
  styleUrls: ['./aboutme.component.scss']
})
export class AboutmeComponent implements OnInit {

  currentUser: User;

  constructor(
    private userService: UserService
  ) {
    this.userService.activeUserObservable.subscribe(user => {
      this.currentUser = user;
    });
  }



  ngOnInit() {
  }

}
