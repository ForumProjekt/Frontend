import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ThreadsService } from '../../../services/threads.service';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';
import { Thread } from '../../../models/thread';

@Component({
  selector: 'app-thread-view',
  templateUrl: './thread-view.component.html',
  styleUrls: ['./thread-view.component.scss']
})
export class ThreadViewComponent implements OnInit {

  private threadID: number;
  private currentUser: User;
  constructor(
    private route: ActivatedRoute,
    private threadsService: ThreadsService,
    private userService: UserService
  ) {
    this.route.params.subscribe(params => {
      this.threadID = +params['threadID']; // (+) converts string 'id' to a number 
      this.threadsService.getThreadsByID(this.threadID);
    });
    this.userService.activeUserObservable.subscribe(user => {
      this.currentUser = user;
    });
  }

  get thread(): Thread {
    return this.threadsService.thread;
  }

  get threads() {
    return this.threadsService.threads;
  }

  get getCurrentUser(): User {
    return this.currentUser;
  }

  get threadIDNumber(): number {
    return this.threadID;
  }



  ngOnInit() {
  }

}
