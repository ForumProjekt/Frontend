import { Component, OnInit } from '@angular/core';
import { ThreadsService } from '../../services/threads.service';

@Component({
  selector: 'app-threads',
  templateUrl: './threads.component.html',
  styleUrls: ['./threads.component.scss']
})
export class ThreadsComponent implements OnInit {


  constructor(
    private threadsService: ThreadsService
  ) { }

  get threads() {
    return this.threadsService.threads;
  }

  callthreadByID(ID: number) {
    this.threadsService.getThreadsByID(ID);
  }


  ngOnInit() {
  }

}
