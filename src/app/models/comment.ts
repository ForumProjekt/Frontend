export class Comment {

    constructor(
        public commentID: number,
        public titel: string,
        public commentText: string,
        public upvotes: number,
        public downVotes: number,
        public views: number,
        public creationDate: Date,
        public userID: string,
        public changes?: string,
        public username?: string,
        public email?: string,
        public registeredSince?: Date,
        public label?: string,
        public hexcolor?: string
    ) {
    }
}
