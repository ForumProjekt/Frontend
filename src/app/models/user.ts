export class User {
  constructor(
    public uuid?: string,
    public username?: string,
    public password?: string,
    public passwordConfirm?: string,
    public email?: string,
    public registeredSince?: Date,
    public firstname?: string,
    public lastname?: string,
    public birthdate?: Date,
    public biography?: string,
    public location?: string,
    public homepage?: string,
    public level?: number,
    public rank?: number
  ) { }

}
