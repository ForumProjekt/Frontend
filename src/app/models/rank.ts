export class Rank {

  constructor(
    public id: number,
    public label: string,
    public hexcolor?: string
  ) { }

}
