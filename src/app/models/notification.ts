export class Notification {

    constructor(
        public notificationId?: number,
        public notificationText?: string,
        public notificationDate?: Date,
        public notificationType?: number,
    ) {
    }
}
