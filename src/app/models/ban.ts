export class Ban {

    constructor(
        public id?: number,
        public userID?: string,
        public reason?: string,
        public permanent?: boolean,
        public expiresAt?: Date,
        public username?: string
    ) {
    }

}
