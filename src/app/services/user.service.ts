import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import * as moment from 'moment';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SystemAlertsService } from './system-alerts.service';
import { Rank } from '../models/rank';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class UserService {
  private options = {
    headers: new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    )
  };

  private config = require('assets/Config.json');
  private currentSystemUser: BehaviorSubject<User> = new BehaviorSubject<User>(
    new User()
  );
  private syncIndicatorHTTP = false;
  private syncIndicatorTIMER = false;
  private userDataCache: User[] = [];
  private activeEditUser: User;

  constructor(
    private http: HttpClient,
    public systemAlertService: SystemAlertsService
  ) {
    this.initialLoadFromDB();
    // try {
    //   // this.currentSystemUser.next(JSON.parse(localStorage.getItem('currentSystemUser')));
    //   // this.activeUser = new User(localStorage.getItem('uuid'));
    //   console.log('loaded from localStorage into observable' +
    //    this.currentSystemUser.subscribe(user => console.log(JSON.stringify(user))));
    // } catch (error) {
    //   console.log('Not logged in, error is: '
    //     + error + ' JsonString to pare loaded from localStorage --> '
    //     + localStorage.getItem('currentSystemUser'));
    // }
  }

  openAdminUserEdit(userEntry: User) {
    this.activeEditUser = userEntry;
  }

  updateUserProfile(userEntry: User) {
    const _body = new URLSearchParams();
    _body.set('uuid', userEntry.uuid);
    _body.set('username', userEntry.username);
    _body.set('password', userEntry.password);
    _body.set('email', userEntry.email);
    _body.set('firstname', userEntry.firstname);
    _body.set('lastname', userEntry.lastname);
    _body.set('birthdate', this.convertDate(userEntry.birthdate));
    _body.set('biography', userEntry.biography);
    _body.set('location', userEntry.location);
    _body.set('homepage', userEntry.homepage);
    _body.set('level', JSON.stringify(userEntry.level));
    _body.set('rank', JSON.stringify(userEntry.rank));

    this.http
      .patch(
        this.config.BackendURL + 'users/' + userEntry.uuid,
        _body.toString(),
        this.options
      )
      .subscribe((error: any) => {
        console.log(error);
      });
  }

  convertDate(datum: Date): string {
    console.log(datum);
    const converted = moment(datum).format('YYYY-MM-DD');
    console.log(converted);
    return converted;
  }

  updateRank(userEntry: User, rank: Rank) {
    const _body = new URLSearchParams();
    _body.set('rank', JSON.stringify(rank.id));

    this.http
      .patch(
        this.config.BackendURL + 'users/updateRank/' + userEntry.uuid,
        _body.toString(),
        this.options
      )
      .subscribe((error: any) => {
        console.log(error);
      });
  }

  updateCache() {
    this.syncUsers();
  }

  get activeUserObservable(): Observable<User> {
    return this.currentSystemUser.asObservable();
  }

  get activeUser(): User {
    this.currentSystemUser.subscribe(user => {
      return user;
    });
    return null;
  }

  set activeUser(user: User) {
    let tmpArray: User[] = [];
    tmpArray = this.userDataCache.filter(u => u.uuid === user.uuid);
    this.currentSystemUser.next(tmpArray[0]);
    this.currentSystemUser.subscribe(userrr =>
      localStorage.setItem('currentSystemUser', JSON.stringify(userrr))
    );
  }

  get getUsers(): User[] {
    return this.userDataCache;
  }

  get rotateActive(): boolean {
    if (this.syncIndicatorHTTP || this.syncIndicatorTIMER) {
      return true;
    } else {
      return false;
    }
  }

  get currentlyEditingUser(): User {
    return this.activeEditUser;
  }

  private initialLoadFromDB() {
    // Get active logged in User from DB
    this.http
      .get(this.config.BackendURL + 'users/' + localStorage.getItem('uuid'))
      .subscribe(
        data => {
          this.currentSystemUser.next(<User>data);
        },
        err => {
          this.systemAlertService.display(
            'Konnte eingeloggten Nutzer nicht zuordnen!',
            'danger'
          );
          console.log('[ERROR] [USER SERVICE] ' + err);
        }
      );
    // Get all users from DB
    this.syncUsers();
  }

  private syncUsers() {
    this.runSyncIndicator();
    // Alle Nutzer aus DB holen
    this.http.get(this.config.BackendURL + 'users').subscribe(
      data => {
        this.userDataCache = <User[]>data;
        this.syncIndicatorHTTP = false;
      },
      err => {
        this.systemAlertService.display(
          'Benutzerliste konnte nicht geladen werden',
          'danger'
        );
        // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
        console.log('[ERROR] [USER SERVICE] ' + err);
      }
    );
  }

  private runSyncIndicator() {
    this.syncIndicatorHTTP = true;
    this.syncIndicatorTIMER = true;
    setTimeout(
      function() {
        this.syncIndicatorTIMER = false;
      }.bind(this),
      1000
    );
  }
}
