import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import * as moment from 'moment';
import { User } from '../models/user';
import { Router } from '@angular/router';
import { ModalService } from './modal.service';
import { UserService } from './user.service';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class AuthService {

  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  public config = require('assets/Config.json');
  private loadingModalHTTP = false;
  private loadingModalTIMER = false;
  private currentUserPath = '/';

  constructor(
    private http: HttpClient,
    private router: Router,
    public modalService: ModalService,
    private userService: UserService
  ) { }

  set currentPath(path: string) {
    this.currentUserPath = path;
  }

  login(_user: User) {
    // Activate loading Modal
    this.modalService.activateLoadingModal();
    const _body = new URLSearchParams();
    _body.set('username', _user.username);
    _body.set('password', _user.password);

    return this.http.post(this.config.BackendURL + 'auth/login', _body.toString(), this.options).toPromise()
      .then((response: any) => {
        console.log(response);
        if (response && response.idToken) {
          this.setSession(response);
        }
      })
      .catch(error => {
        if (error.status !== 200) {
          this.modalService.loadingModalHTTPStatus = false;
        }
      });
  }

  public setSession(_authResult) {
    const _expiresAt = moment().add(_authResult.expiresIn, 'second');
    if (_authResult.expiresIn !== undefined || _authResult.idToken !== undefined) {
      console.log('[INFO] logged in - welcome back!');
      localStorage.setItem('id_token', _authResult.idToken);
      localStorage.setItem('uuid', _authResult.uuid);
      localStorage.setItem('expires_at', JSON.stringify(_expiresAt.valueOf()));

      // Set current user
      this.userService.activeUser = new User(_authResult.uuid);
      // Stop loading Modal
      this.modalService.loadingModalHTTPStatus = false;
      // Navigate user to Index
      this.router.navigate([this.currentUserPath]);
      // TODO: Alert Service Login Info
    }
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    console.log('[INFO] logged out - bye!');
  }

  loggedIn() {
    let expiration;
    try {
      expiration = this.getExpiration();
    } catch (error) {
      console.log('Can\'t get Expiration from JWT - Is JWT in localStorage?');
      return false;
    }
    return moment().isBefore(expiration);
  }

  getExpiration() {
    const _expiration = localStorage.getItem('expires_at');
    const _expiresAt = JSON.parse(_expiration);
    return moment(_expiresAt);
  }

  register(_user: User) {
    moment.locale();
    moment().format('l');
    const _body = new URLSearchParams();
    _body.set('username', _user.username);
    _body.set('password', _user.password);
    _body.set('email', _user.email);
    _body.set('firstname', _user.firstname);
    _body.set('lastname', _user.lastname);
    _body.set('birthdate', this.convertDate(_user.birthdate));
    _body.set('biography', _user.biography);
    _body.set('location', _user.location);
    _body.set('homepage', _user.homepage);

    this.http.post(this.config.BackendURL + 'auth/register', _body.toString(), this.options).subscribe((response: any) => {
      console.log(response);
      if (response) {
        this.login(_user);
      }
      // this.login(_user);
    });

  }

  convertDate(datum: Date): string {
    console.log(datum);
    const converted = moment(datum).format('YYYY-MM-DD');
    console.log(converted);
    return converted;
  }

}
