import { Injectable } from '@angular/core';

@Injectable()
export class SystemAlertsService {

  public alert: any = {
    type: '',
    text: '',
    title: '',
  };
  public options = {
    timeOut: 3000
  };
  private show = false;

  constructor() { }

  display(text: string, type?: string, title?: string) {
    this.alert.text = text;
    if (!type) {
      this.alert.type = 'info';
    } else {
      this.alert.type = type;
    }
    setTimeout(() => {
      this.alert.text = '';
      this.alert.title = '';
    }, this.options.timeOut);
  }

  get text(): string {
    return this.alert.text;
  }

  get title(): string {
    return this.alert.title;
  }

  get type(): string {
    return this.alert.type;
  }

}
