import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as moment from 'moment';
import { ThreadsComponent } from '../components/threads/threads.component';
import { ThreadViewComponent } from '../components/threads/thread-view/thread-view.component';
import { User } from '../models/user';
import { Thread } from '../models/thread';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SystemAlertsService } from './system-alerts.service';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class ThreadsService implements OnInit {

  private config = require('assets/Config.json');

  threadDataCache: Thread[];
  singleThreadDataCache = new Thread(0, 'titleHey', 'text', 0, 0, 0, new Date(), '');
  threadData: Thread;
  trd: Thread[] = [];
  threadId: number;
  threadCreator: User;
  options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };

  constructor(
    private http: HttpClient,
    private alertsService: SystemAlertsService
  ) {
    this.getThreads();
  }


  newThread(threadEntry: Thread, tags: string[]) {

    // let createdThreadID: number;
    const createdThreadID = new BehaviorSubject(0);
    // let currentSystemUser: BehaviorSubject<number> = new BehaviorSubject<number>(new Object());

    const _body = new URLSearchParams();
    _body.set('titel', threadEntry.titel);
    _body.set('threadText', threadEntry.threadText);
    _body.set('userID', threadEntry.userID);

    this.http.post(this.config.BackendURL + 'threads', _body.toString(), this.options).subscribe(
      async (data: any) => {
        console.log('Thread created with ID ' + JSON.stringify(data.threadID));
        createdThreadID.next(JSON.parse(data.threadID));
        // createdThreadID = JSON.parse(data.threadID);
      }, (error: any) => {
      console.log(error);
    });

    const _tagBody = new URLSearchParams();
    _tagBody.set('tagLabels', tags.join(','));

    createdThreadID.subscribe((id) => {

      this.http.post(this.config.BackendURL + 'tags/' + id, _tagBody.toString(), this.options).subscribe((error: any) => {
        console.log(error);
      });

    });

    this.alertsService.display('Thread erfolgreich erstellt', 'success');
    this.getThreads();

  }

  get threads(): Thread[] {
    return this.trd;
  }

  get thread(): Thread {
    return this.threadData;
  }

  getThreads() {
    this.http.get(this.config.BackendURL + 'threads').subscribe(data => {
      this.trd = <Thread[]>data;
    }, err => {
      console.log(err);
    });
  }

  getThreadsByID(ID: number): Thread {
    if (this.singleThreadDataCache.threadID !== ID) {
      this.getSingleThreadFromDB(ID);
    }
    return this.singleThreadDataCache;
  }

  getUserByThread(): User {
    return this.threadCreator;
  }

  getSingleThreadFromDB(id: number) {
    this.http.get(this.config.BackendURL + 'threads/' + id).subscribe(data => {
      this.singleThreadDataCache = <Thread>data;
      this.threadData = <Thread>data;
    }, err => {
      // this.messageBoxService.display('Ladefehler', 'Threads konnten nicht geladen werden - Fehler: ' + err.status, 'error');
      console.log(err);
    });
  }

  initialLoadFromDB() {
    this.http.get(this.config.BackendURL + 'threads').subscribe(data => {
      this.threadDataCache = <Thread[]>data;
    }, err => {
      // this.messageBoxService.display('Ladefehler', 'Threads konnten nicht geladen werden - Fehler: ' + err.status, 'error');
      console.log(err);
    });
  }

  ngOnInit() {

  }

}
