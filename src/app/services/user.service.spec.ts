import { TestBed, inject } from '@angular/core/testing';

import { UserService } from './user.service';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppModule } from '../app.module';

describe('UserService', () => {
  let uservice: UserService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        UserService
      ]
    });
  });

  beforeEach(inject([UserService], (service: UserService) => {
    uservice = service;
  }));

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
});
