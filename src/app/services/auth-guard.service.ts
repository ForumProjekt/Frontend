import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService {

  constructor(private _auth: AuthService, private _router: Router) { }

  canActivate() {
    if (this._auth.loggedIn()) {
      return true;
    } else {
      this._router.navigate(['unauthorized']);
      return false;
    }
  }

}
