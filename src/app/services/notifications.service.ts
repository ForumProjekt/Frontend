import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Notification } from '../models/notification';

declare function require(url: string);

@Injectable()
export class NotificationService {

  private options = {
    headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
  };
  private config = require('assets/Config.json');
  notificationDataCache: Notification[];
  notificationData: Notification;

  constructor(private http: HttpClient) {
    this.initialLoadFromDB();
  }

  get notifications(): Notification[] {
    return this.notificationDataCache;
  }

  initialLoadFromDB() {
    this.http.get(this.config.BackendURL + 'notifications').subscribe(data => {
      this.notificationDataCache = <Notification[]>data;
      console.log(data);
    }, err => {
      // this.messageBoxService.display('Ladefehler', 'Threads konnten nicht geladen werden - Fehler: ' + err.status, 'error');
      console.log(err);
    });
  }

  syncNotifications() {
    this.http.get(this.config.BackendURL + 'notifications').subscribe(data => {
      this.notificationDataCache = <Notification[]>data;
      console.log(data);
    }, err => {
      // this.messageBoxService.display('Ladefehler', 'Threads konnten nicht geladen werden - Fehler: ' + err.status, 'error');
      console.log(err);
    });
  }

  addNotification(notification: Notification) {
    const _body = new URLSearchParams();
    console.log(notification.notificationText);
    _body.set('notificationText', notification.notificationText);
    console.log(JSON.stringify(_body.get('notificationText')));
    this.http.post(this.config.BackendURL + 'notifications', _body.toString(), this.options).subscribe((error: any) => {
      console.log(error);
    });
  }

  deleteNotification(id: number) {
    this.http.delete(this.config.BackendURL + 'notifications/' + id);
  }
}
