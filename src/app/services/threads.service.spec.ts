import { TestBed, inject } from '@angular/core/testing';

import { ThreadsService } from './threads.service';
import { APP_BASE_HREF } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';

describe('ThreadsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    });
  });

  it('should be created', inject([ThreadsService], (service: ThreadsService) => {
    expect(service).toBeTruthy();
  }));
});
