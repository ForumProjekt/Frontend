import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Comment } from '../models/comment';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class CommentService {

  public cmnt: Comment[];
  public threadId: number;
  private config = require('assets/Config.json');
  public gotComments = false;

  constructor(
    private http: HttpClient
  ) {
  }

  get comments(): Comment[] {
    return this.cmnt;
  }

  getComments(threadID: number) {
    this.threadId = threadID;
    this.http.get(this.config.BackendURL + 'threads/' + threadID).subscribe(data => {
      console.log(JSON.stringify(data));
      this.cmnt = <Comment[]>data;
      if (this.cmnt === undefined) {
        this.cmnt = [];
      }
      this.gotComments = true;
    }, err => {
      console.log(err);
    });
  }
}
