import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Ban } from '../models/ban';
import { SystemAlertsService } from './system-alerts.service';

@Injectable()
export class BanService {

  private options = {
    headers: new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    )
  };
  private config = require('assets/Config.json');
  private syncIndicatorHTTP = false;
  private syncIndicatorTIMER = false;
  private banDataCache: Ban[] = [];
  private activeBanEntry: Ban;

  constructor(
    private http: HttpClient,
    public systemAlertService: SystemAlertsService
  ) {
    this.syncBans();
  }

  get getBans(): Ban[] {
    return this.banDataCache;
  }

  get currentlyEditingBan(): Ban {
    return this.activeBanEntry;
  }

  get rotateActive(): boolean {
    if (this.syncIndicatorHTTP || this.syncIndicatorTIMER) {
      return true;
    } else {
      return false;
    }
  }

  addBan(banEntry: Ban) {

    const _body = new URLSearchParams();
    _body.set('userID', banEntry.userID);
    _body.set('reason', banEntry.reason);
    _body.set('permanent', JSON.stringify(banEntry.permanent));
    if (!banEntry.permanent) {
      _body.set('expiresAt', JSON.stringify(banEntry.expiresAt));
    }

    this.http
      .post(
        this.config.BackendURL + 'bans/', _body.toString(), this.options
      ).subscribe((response: any) => {
        console.log(response);
        if (response) {
          this.systemAlertService.display('User erfolgreich gebannt!', 'success');
        }
      }
      );

  }

  updateBan(banEntry: Ban) {

    const _body = new URLSearchParams();
    _body.set('id', JSON.stringify(banEntry.id));
    _body.set('userID', banEntry.userID);
    _body.set('reason', banEntry.reason);
    _body.set('permanent', JSON.stringify(banEntry.permanent));
    _body.set('expiresAt', JSON.stringify(banEntry.expiresAt));

    this.http
      .patch(
        this.config.BackendURL + 'bans/' + banEntry.id,
        _body.toString(),
        this.options
      )
      .subscribe((error: any) => {
        console.log(error);
      });

  }

  deleteBan(banEntry: Ban) {

    this.http
      .delete(
        this.config.BackendURL + 'bans/' + banEntry.userID, this.options
      )
      .subscribe((error: any) => {
        console.log(error);
        this.systemAlertService.display(error, 'danger', 'Fehler beim Entbannen');
      });
  }

  openAdminBanEdit(banEntry: Ban) {
    this.activeBanEntry = banEntry;
  }

  updateCache() {
    this.syncBans();
  }

  private syncBans() {
    this.runSyncIndicator();
    // Alle Bans aus DB holen
    this.http.get(this.config.BackendURL + 'bans').subscribe(
      data => {
        this.banDataCache = <Ban[]>data;
        this.syncIndicatorHTTP = false;
      },
      err => {
        this.systemAlertService.display(
          'Banliste konnten nicht geladen werden',
          'danger'
        );
        console.log('[ERROR] [BAN SERVICE] ' + err);
      }
    );
  }

  private runSyncIndicator() {
    this.syncIndicatorHTTP = true;
    this.syncIndicatorTIMER = true;
    setTimeout(
      function () {
        this.syncIndicatorTIMER = false;
      }.bind(this),
      1000
    );
  }

}
