import { TestBed, inject } from '@angular/core/testing';

import { SystemAlertsService } from './system-alerts.service';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AppModule } from '../app.module';

describe('SystemAlertsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    });
  });

  it('should be created', inject([SystemAlertsService], (service: SystemAlertsService) => {
    expect(service).toBeTruthy();
  }));
});
