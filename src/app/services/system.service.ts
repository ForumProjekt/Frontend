import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class SystemService {

  private config = require('assets/Config.json');
  private syncIndicatorHTTP = false;
  private syncIndicatorTIMER = false;
  private cpus = '';
  private avgLoad = {
    'avg1': '',
    'avg10': '',
    'avg15': ''
  };
  private freeRam = '';
  private maxRam = '';
  private usedRam = '';
  private backendOS = '';
  private uptime = '';

  constructor(
    private http: HttpClient
  ) {
    this.syncData();
  }

  get cpuThreads() {
    return this.cpus;
  }

  get maxMem() {
    return this.maxRam;
  }

  get usedMem() {
    return this.usedRam;
  }

  get freeMem() {
    return this.freeRam;
  }

  get hostOS() {
    return this.backendOS;
  }

  get backendUptime() {
    return this.uptime;
  }

  get backendAvgLoad() {
    return this.avgLoad;
  }

  private syncData() {
    this.runSyncIndicator();
    // Average CPU Load on Backend
    this.http.get(this.config.BackendURL + 'system/avgLoad').subscribe(data => {
      this.avgLoad.avg1 = <any>data['avg1'];
      this.avgLoad.avg10 = <any>data['avg10'];
      this.avgLoad.avg15 = <any>data['avg15'];
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });
    // Backend Host OS
    this.http.get(this.config.BackendURL + 'system/hostOS').subscribe(data => {
      const ho = <any>data + '';
      if (ho === 'win32') {
        this.backendOS = 'Windows';
      } else if (ho === 'linux') {
        this.backendOS = 'Linux';
      } else if (ho === 'darwin') {
        this.backendOS = 'MacOS';
      } else {
        this.backendOS = ho + '';
      }
    }, err => {

      // TODO: FIX: Geht nur bei Fehler???

      const ho = <any>err.error.text + '';
      if (ho === 'win32') {
        this.backendOS = 'Windows';
      } else if (ho === 'linux') {
        this.backendOS = 'Linux';
      } else if (ho === 'darwin') {
        this.backendOS = 'MacOS';
      } else {
        this.backendOS = ho + '';
      }
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      // console.log('[ERROR] [SYSTEM SERVICE] Can\'t load hostOS: ' + JSON.stringify(err));
    });
    // Uptime
    this.http.get(this.config.BackendURL + 'system/uptime').subscribe(data => {
      this.uptime = <any>data;
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });
    // Free Ram
    this.http.get(this.config.BackendURL + 'system/freeRam').subscribe(data => {
      this.freeRam = <any>data;
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });
    // Used Ram
    this.http.get(this.config.BackendURL + 'system/usedRam').subscribe(data => {
      this.usedRam = <any>data;
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });
    // Max Ram
    this.http.get(this.config.BackendURL + 'system/maxRam').subscribe(data => {
      this.maxRam = <any>data;
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });
    // # CPU Threads
    this.http.get(this.config.BackendURL + 'system/cpus').subscribe(data => {
      this.cpus = <any>data;
    }, err => {
      // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
      console.log('[ERROR] [SYSTEM SERVICE] ' + err);
    });

    this.syncIndicatorHTTP = false;
  }

  private runSyncIndicator() {
    this.syncIndicatorHTTP = true;
    this.syncIndicatorTIMER = true;
    setTimeout(function () {
      this.syncIndicatorTIMER = false;
    }.bind(this), 1000);
  }

}
