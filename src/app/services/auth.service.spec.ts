import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AppModule } from '../app.module';
import { APP_BASE_HREF } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ModalService } from './modal.service';
import { User } from '../models/user';

describe('AuthService', () => {
  let aservice: AuthService;
  let http: HttpClient;
  let backend: HttpTestingController;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([]), HttpClientModule, HttpClientTestingModule],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' },
        AuthService
      ]
    });
    aservice = TestBed.get(AuthService);
    http = TestBed.get(HttpClient);
    backend = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(aservice).toBeTruthy();
  });

  it('should login an user', () => {
    spyOn(aservice.modalService, 'activateLoadingModal');
    spyOn(aservice, 'setSession');
    aservice.login(new User()).then(() => {
      expect(aservice.setSession).toHaveBeenCalled();
      expect(aservice.modalService.activateLoadingModal).toHaveBeenCalled();
    });
    backend.expectOne(aservice.config.BackendURL + 'auth/login').
      flush({ idToken: 'foo' }, { status: 200, statusText: 'foo' });

  });

  it('should deactivate loading Modal on api error', () => {
    spyOn(aservice.modalService, 'activateLoadingModal');
    spyOn(aservice, 'setSession');
    aservice.login(new User()).then(() => {
      expect(aservice.setSession).toHaveBeenCalledTimes(0);
      expect(aservice.modalService.activateLoadingModal).toHaveBeenCalled();
      expect(aservice.modalService.loadingModalHTTP).toBeFalsy();
    });
    backend.expectOne(aservice.config.BackendURL + 'auth/login').
      flush({ idToken: 'foo' }, { status: 405, statusText: 'foo' });

  });

});
