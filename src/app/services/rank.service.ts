import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { SystemAlertsService } from './system-alerts.service';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Rank } from '../models/rank';

// custom require func for json config import
declare function require(url: string);

@Injectable()
export class RankService {

  private options = {
    headers: new HttpHeaders().set(
      'Content-Type',
      'application/x-www-form-urlencoded'
    )
  };
  private config = require('assets/Config.json');
  private syncIndicatorHTTP = false;
  private syncIndicatorTIMER = false;
  private rankDataCache: Rank[] = [];

  constructor(
    private http: HttpClient,
    public systemAlertService: SystemAlertsService
  ) {
    this.syncRanks();
  }

  get getRanks(): Rank[] {
    return this.rankDataCache;
  }

  get rotateActive(): boolean {
    if (this.syncIndicatorHTTP || this.syncIndicatorTIMER) {
      return true;
    } else {
      return false;
    }
  }

  updateRank(rankEntry: Rank) {

    const _body = new URLSearchParams();
    _body.set('id', JSON.stringify(rankEntry.id));
    _body.set('label', rankEntry.label);
    _body.set('hexcolor', rankEntry.hexcolor);

    this.http
      .patch(
        this.config.BackendURL + 'ranks/' + rankEntry.id,
        _body.toString(),
        this.options
      )
      .subscribe((error: any) => {
        console.log(error);
      });

  }

  updateCache() {
    this.syncRanks();
  }

  private syncRanks() {
    this.runSyncIndicator();
    // Alle Nutzer aus DB holen
    this.http.get(this.config.BackendURL + 'ranks').subscribe(
      data => {
        this.rankDataCache = <Rank[]>data;
        this.syncIndicatorHTTP = false;
      },
      err => {
        this.systemAlertService.display(
          'Ränge konnten nicht geladen werden',
          'danger'
        );
        // this.messageBoxService.error('Ladefehler', 'Benutzer konnten nicht geladen werden - Fehler: ' + err.status);
        console.log('[ERROR] [RANK SERVICE] ' + err);
      }
    );
  }

  private runSyncIndicator() {
    this.syncIndicatorHTTP = true;
    this.syncIndicatorTIMER = true;
    setTimeout(
      function () {
        this.syncIndicatorTIMER = false;
      }.bind(this),
      1000
    );
  }

}
