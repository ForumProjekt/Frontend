import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { UserService } from './user.service';
import { Rank } from '../models/rank';
import { RankService } from './rank.service';
import { BanService } from './ban.service';
import { Ban } from '../models/ban';

@Injectable()
export class ModalService {
  // Loading
  public loadingModalHTTP = false;
  public loadingModalTIMER = false;
  // ADMIN Edit User
  private editUserModalActive = false;
  private addNewNotification = false;
  private editUser: User;
  // ADMIN Edit Rank
  private editRankModalActive = false;
  private editRank: Rank;
  // ADMIN Edit Ban
  private editBanModalActive = false;
  private editBan: Ban;

  constructor(
    private userService: UserService,
    private rankService: RankService,
    private banService: BanService
  ) { }

  /*
   * Login Loading Modal
   */

  activateLoadingModal() {
    this.loadingModalHTTP = true;
    this.loadingModalTIMER = true;
    setTimeout(function () {
      this.loadingModalTIMER = false;
    }.bind(this), 1000);
  }

  set loadingModalHTTPStatus(status: boolean) {
    this.loadingModalHTTP = status;
  }

  get loadingModalStatus() {
    if (this.loadingModalHTTP || this.loadingModalTIMER) {
      return true;
    } else {
      return false;
    }
  }

  /*
   * Edit User Modal
   */

  openAdminEditUserModal(user: User) {
    this.editUserModalActive = true;
    this.editUser = user;
    console.log(JSON.stringify(user));
  }

  get adminEditUserModalStatus() {
    return this.editUserModalActive;
  }

  set adminEditUser(user: User) {
    this.editUser = user;
  }

  get adminEditUser() {
    return this.editUser;
  }

  saveAdminEditUser() {
    this.userService.updateUserProfile(this.editUser);
    this.closeAdminEditUserModal();
  }

  closeAdminEditUserModal() {
    this.editUserModalActive = false;
  }

  /*
   * Edit Rank Modal
   */

  openAdminEditRankModal(rank: Rank) {
    this.editRankModalActive = true;
    this.editRank = rank;
    console.log(JSON.stringify(rank));
  }

  get adminEditRankModalStatus() {
    return this.editRankModalActive;
  }

  set adminEditRank(rank: Rank) {
    this.editRank = rank;
  }

  get adminEditRank() {
    return this.editRank;
  }

  saveAdminEditRank() {
    this.rankService.updateRank(this.editRank);
    this.closeAdminEditRankModal();
  }

  closeAdminEditRankModal() {
    this.editRankModalActive = false;
  }

  /*
   * Edit Ban Modal
   */

  openAdminEditBanModal(ban: Ban) {
    this.editBanModalActive = true;
    this.editBan = ban;
    console.log('[modalService] opened ban editing modal with object: ' + JSON.stringify(ban));
  }

  get adminEditBanModalStatus() {
    return this.editBanModalActive;
  }

  set adminEditBan(ban: Ban) {
    this.editBan = ban;
  }

  get adminEditBan() {
    return this.editBan;
  }

  saveAdminEditBan() {
    this.banService.updateBan(this.editBan);
    this.closeAdminEditBanModal();
  }

  closeAdminEditBanModal() {
    this.editBanModalActive = false;
    console.log('[modalService] closed ban editing modal');
  }

}
