import { TestBed, inject } from '@angular/core/testing';

import { ModalService } from './modal.service';
import { AppModule } from '../app.module';
import { RouterModule } from '@angular/router';
import { APP_BASE_HREF } from '@angular/common';

describe('ModalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, RouterModule.forRoot([])],
      providers: [
        { provide: APP_BASE_HREF, useValue: '/' }
      ]
    });
  });

  it('should be created', inject([ModalService], (service: ModalService) => {
    expect(service).toBeTruthy();
  }));

  it('should activateLoadingModal', inject([ModalService], (service: ModalService) => {
    jasmine.clock().install();
    service.activateLoadingModal();
    expect(service.loadingModalHTTP).toBeTruthy();
    expect(service.loadingModalTIMER).toBeTruthy();
    // ZEIT
    jasmine.clock().tick(1001);
    expect(service.loadingModalTIMER).toBeFalsy();
    jasmine.clock().uninstall();


  }));

});
