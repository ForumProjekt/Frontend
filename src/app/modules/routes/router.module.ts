import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from '../../components/homepage/homepage.component';
import { ProfileComponent } from '../../components/profile/profile.component';
import { AppModule } from '../../app.module';
import { AppComponent } from '../../app.component';
import { EditProfileComponent } from '../../components/profile/edit-profile/edit-profile.component';
import { ThreadsComponent } from '../../components/threads/threads.component';
import { NewThreadsComponent } from '../../components/new-threads/new-threads.component';
import { AboutmeComponent } from '../../components/profile/aboutme/aboutme.component';
import { FollowerComponent } from '../../components/profile/follower/follower.component';
import { LoginComponent } from '../../components/auth/login/login.component';
import { RegisterComponent } from '../../components/auth/register/register.component';
import { ThreadViewComponent } from '../../components/threads/thread-view/thread-view.component';
import { AdminOverviewComponent } from '../../components/admin/admin-overview/admin-overview.component';
import { SystemOverviewComponent } from '../../components/admin/system/system-overview/system-overview.component';
import { SystemConfigurationComponent } from '../../components/admin/system/system-configuration/system-configuration.component';
import { AdminThreadCategoriesComponent } from '../../components/admin/threads/admin-thread-categories/admin-thread-categories.component';
import { AdminThreadCommentsComponent } from '../../components/admin/threads/admin-thread-comments/admin-thread-comments.component';
import { AdminThreadOverviewComponent } from '../../components/admin/threads/admin-thread-overview/admin-thread-overview.component';
import { AdminThreadTagsComponent } from '../../components/admin/threads/admin-thread-tags/admin-thread-tags.component';
import { AdminUserOverviewComponent } from '../../components/admin/users/admin-user-overview/admin-user-overview.component';
import { AdminUserRanksComponent } from '../../components/admin/users/admin-user-ranks/admin-user-ranks.component';
import { AdminUserBansComponent } from '../../components/admin/users/admin-user-bans/admin-user-bans.component';
import { UserRoutesGuard } from '../../guards/user-routes.guard';
import { RankRoutesGuard } from '../../guards/rank-routes.guard';
import { DeleteThreadComponent } from '../../components/threads/delete-thread/delete-thread.component';
import { SystemNotificationsComponent } from '../../components/admin/system/system-notifications/system-notifications.component';

export const routes: Routes = [
  // Start
  {
    path: 'home',
    component: HomepageComponent
  },
  // Login
  {
    path: 'auth/login',
    component: LoginComponent
  },
  // Registrieren
  {
    path: 'auth/register',
    component: RegisterComponent
  },
  // Profil
  {
    path: 'profile/about',
    component: AboutmeComponent,
    canActivate: [UserRoutesGuard]
  },
  // Profil Bearbeiten
  {
    path: 'profile/edit',
    component: EditProfileComponent,
    canActivate: [UserRoutesGuard]
  },
  // Follower verwalten
  {
    path: 'profile/follower',
    component: FollowerComponent,
    canActivate: [UserRoutesGuard]
  },
  // Fremdes Profil
  {
    path: 'profile/:userID',
    component: ProfileComponent,
    canActivate: [UserRoutesGuard]
  },
  // Fremde Follower
  {
    path: 'profile/:userID/follower',
    component: FollowerComponent,
    canActivate: [UserRoutesGuard]
  },
  // Threads
  {
    path: 'threads',
    component: ThreadsComponent
  },
  // create Thread
  {
    path: 'threads/create',
    component: NewThreadsComponent,
    canActivate: [UserRoutesGuard]
  },
  {
    path: 'threads/:threadID',
    component: ThreadViewComponent
  },
  // delete Thread
  {
    path: 'threads/:threadID/delete',
    component: DeleteThreadComponent,
    canActivate: [UserRoutesGuard]
  },
  // Registrieren
  {
    path: 'auth/register',
    component: RegisterComponent
  },
  //////////////
  /* Admin CP */
  //////////////
  // Übersicht
  {
    path: 'admin/overview',
    component: AdminOverviewComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // System Übersicht
  {
    path: 'admin/system/overview',
    component: SystemOverviewComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  //System Notifications
  {
    path: 'admin/system/notifications',
    component: SystemNotificationsComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // System Konfiguration
  {
    path: 'admin/system/configuration',
    component: SystemConfigurationComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // Thread Kategorien
  {
    path: 'admin/threads/categories',
    component: AdminThreadCategoriesComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // Thread Übersicht
  {
    path: 'admin/threads/overview',
    component: AdminThreadOverviewComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // Thread Kommentare
  {
    path: 'admin/threads/comments',
    component: AdminThreadCommentsComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // Thread Tags
  {
    path: 'admin/threads/tags',
    component: AdminThreadTagsComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '10'
    }
  },
  // User Übersicht
  {
    path: 'admin/users/overview',
    component: AdminUserOverviewComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '11'
    }
  },
  // User Ränge
  {
    path: 'admin/users/ranks',
    component: AdminUserRanksComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '11'
    }
  },
  // User Banliste
  {
    path: 'admin/users/bans',
    component: AdminUserBansComponent,
    canActivate: [RankRoutesGuard],
    data: {
      expectedRankID: '11'
    }
  },
  /*****************/
  ////////////////////////
  /* REDIRECTION ROUTES */
  ////////////////////////
  // admin > admin/overview
  {
    path: 'admin',
    redirectTo: 'admin/overview',
    pathMatch: 'full'
  },
  // login > auth/login
  {
    path: 'login',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  // register > auth/register
  {
    path: 'register',
    redirectTo: 'auth/register',
    pathMatch: 'full'
  },
  // / > home
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  /*****************/
  // nicht vorhandene Routes zu NotFoundComponent
  {
    path: '**',
    component: HomepageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes, { useHash: false })
  ],
  exports: [
    RouterModule
  ]
})

export class RoutingModule { }
