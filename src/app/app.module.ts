import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { MyDatePickerModule } from 'mydatepicker';
import { AvatarModule } from 'ngx-avatar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ColorPickerModule } from 'ngx-color-picker';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';

import { AppComponent } from './app.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { RoutingModule } from './modules/routes/router.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProfileComponent } from './components/profile/profile.component';
import { FollowerComponent } from './components/profile/follower/follower.component';
import { EditProfileComponent } from './components/profile/edit-profile/edit-profile.component';
import { ThreadsComponent } from './components/threads/threads.component';
import { AboutmeComponent } from './components/profile/aboutme/aboutme.component';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { NewThreadsComponent } from './components/new-threads/new-threads.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ThreadsService } from './services/threads.service';
import { ThreadViewComponent } from './components/threads/thread-view/thread-view.component';
import { AdminOverviewComponent } from './components/admin/admin-overview/admin-overview.component';
import { AdminMenuComponent } from './components/admin/admin-menu/admin-menu.component';
import { SystemOverviewComponent } from './components/admin/system/system-overview/system-overview.component';
import { SystemConfigurationComponent } from './components/admin/system/system-configuration/system-configuration.component';
import { AdminThreadCategoriesComponent } from './components/admin/threads/admin-thread-categories/admin-thread-categories.component';
import { AdminThreadOverviewComponent } from './components/admin/threads/admin-thread-overview/admin-thread-overview.component';
import { AdminThreadCommentsComponent } from './components/admin/threads/admin-thread-comments/admin-thread-comments.component';
import { AdminThreadTagsComponent } from './components/admin/threads/admin-thread-tags/admin-thread-tags.component';
import { AdminUserOverviewComponent } from './components/admin/users/admin-user-overview/admin-user-overview.component';
import { AdminUserRanksComponent } from './components/admin/users/admin-user-ranks/admin-user-ranks.component';
import { AdminUserBansComponent } from './components/admin/users/admin-user-bans/admin-user-bans.component';
import { UserFilterPipe } from './pipes/user-filter.pipe';
import { UserService } from './services/user.service';
import { ModalsComponent } from './components/modals/modals.component';
import { ModalService } from './services/modal.service';
import { CommentComponent } from './components/comment/comment.component';
import { CommentService } from './services/comment.service';

import { UserRoutesGuard } from './guards/user-routes.guard';
import { RankRoutesGuard } from './guards/rank-routes.guard';
import { SystemAlertsComponent } from './components/system-alerts/system-alerts.component';
import { SystemAlertsService } from './services/system-alerts.service';
import { SystemService } from './services/system.service';
import { TrendsComponent } from './components/trends/trends.component';
import { DeleteThreadComponent } from './components/threads/delete-thread/delete-thread.component';
import { NotificationService } from './services/notifications.service';
import { RankService } from './services/rank.service';
import { BanService } from './services/ban.service';
import { SystemNotificationsComponent } from './components/admin/system/system-notifications/system-notifications.component';

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    NavbarComponent,
    FooterComponent,
    ProfileComponent,
    AboutmeComponent,
    FollowerComponent,
    EditProfileComponent,
    ThreadsComponent,
    LoginComponent,
    RegisterComponent,
    NewThreadsComponent,
    NotificationsComponent,
    ThreadViewComponent,
    AdminOverviewComponent,
    AdminMenuComponent,
    SystemOverviewComponent,
    SystemConfigurationComponent,
    AdminThreadCategoriesComponent,
    AdminThreadOverviewComponent,
    AdminThreadCommentsComponent,
    AdminThreadTagsComponent,
    AdminUserOverviewComponent,
    AdminUserRanksComponent,
    AdminUserBansComponent,
    UserFilterPipe,
    ModalsComponent,
    CommentComponent,
    SystemAlertsComponent,
    TrendsComponent,
    DeleteThreadComponent,
    SystemNotificationsComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    MyDatePickerModule,
    AvatarModule,
    ColorPickerModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger'
    })
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
    AuthService,
    UserRoutesGuard,
    RankRoutesGuard,
    ThreadsService,
    UserService,
    ModalService,
    CommentService,
    SystemAlertsService,
    SystemService,
    CommentService,
    NotificationService,
    RankService,
    BanService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
